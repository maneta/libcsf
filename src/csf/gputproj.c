/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * gputproj.c
$Log: gputproj.c,v $
Revision 1.1.1.1  2000/01/04 21:04:44  cees
Initial import Cees

Revision 2.0  1996/05/23 13:16:26  cees
csf2clean

Revision 1.1  1996/05/23 13:11:49  cees
Initial revision

Revision 1.3  1995/11/01 17:23:03  cees
.

 * Revision 1.2  1994/09/08  17:16:23  cees
 * added c2man docs + small code changes
 *
 * Revision 1.1  1994/08/26  13:33:23  cees
 * Initial revision
 *
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/gputproj.c,v 1.1.1.1 2000/01/04 21:04:44 cees Exp $";
#endif


#include "csf.h"
#include "csfimpl.h"

/* change projection type of map
 * MputProjection type changes the projection type of a map.
 * In version 2, projections are simplified. We only discern between
 * a projection with y increasing (PT_YINCT2B=0) and decreasing (PT_YDECT2B=1) 
 * from top to bottom.
 * All old constants that denote a projection with y decreasing are nonzero.
 * And the old constant that denote a projection with y decreasing (PT_XY) is 0.
 * returns the new projection (PT_YINCT2B or PT_YDECT2B) or MV_UINT2 if an 
 * error occurred.
 *
 * Merrno
 * NOACCESS
 */
CSF_PT MputProjection(
	MAP *map,      /* map handle */
	CSF_PT p)       /* projection type, all nonzero values are mapped to
	                * 1 (PT_YDECT2B) 
	                */
{
	CHECKHANDLE_GOTO(map, error);
	if(! WRITE_ENABLE(map))
	{
		M_ERROR(NOACCESS);
		goto error;
	}
	map->main.projection =  (p) ? PT_YDECT2B : PT_YINCT2B;
	return map->main.projection; 
error:	return(MV_UINT2);
}
