/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * trackmm.c 
   $Log: trackmm.c,v $
   Revision 1.1.1.1  2000/01/04 21:05:11  cees
   Initial import Cees

   Revision 2.1  1996/12/29 19:35:21  cees
   src tree clean up

   Revision 2.0  1996/05/23 13:16:26  cees
   csf2clean

   Revision 1.1  1996/05/23 13:11:49  cees
   Initial revision

   Revision 1.2  1995/11/01 17:23:03  cees
   .

 * Revision 1.1  1994/09/13  10:56:47  cees
 * Initial revision
 *
 */
#ifndef lint  
static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/trackmm.c,v 1.1.1.1 2000/01/04 21:05:11 cees Exp $";
#endif

/********/
/* USES */
/********/

/* libs ext. <>, our ""  */
#include "csf.h"
#include "csfimpl.h"

/* global header (opt.) and trackmm's prototypes "" */


/* headers of this app. modules called */ 

/***************/
/* EXTERNALS   */
/***************/

/**********************/ 
/* LOCAL DECLARATIONS */
/**********************/ 

/*********************/ 
/* LOCAL DEFINITIONS */
/*********************/ 

/******************/
/* IMPLEMENTATION */
/******************/

/* disable automatic tracking of minimum and maximum value 
 * A call to RdontTrackMinMax disables the automatic tracking
 * of the min/max value in succesive cell writes. 
 * If used, one must always
 * use RputMinVal and RputMaxVal to set the correct values.
 */
void RdontTrackMinMax(MAP *m) /* map handle */
{
	m->minMaxStatus = MM_DONTKEEPTRACK;
}
