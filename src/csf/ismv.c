/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * ismv.c
$Log: ismv.c,v $
Revision 1.1.1.1  2000/01/04 21:04:44  cees
Initial import Cees

Revision 2.0  1996/05/23 13:16:26  cees
csf2clean

Revision 1.1  1996/05/23 13:11:49  cees
Initial revision

Revision 1.3  1995/11/01 17:23:03  cees
.

 * Revision 1.2  1994/09/05  13:27:26  cees
 * changed for appCR field
 *
 * Revision 1.1  1994/08/26  13:33:23  cees
 * Initial revision
 *
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/ismv.c,v 1.1.1.1 2000/01/04 21:04:44 cees Exp $";
#endif

/********************************************************/
/*							*/
/*	IsMV						*/
/*							*/
/********************************************************/
/* check if cellValue is a missing value		*/
/********************************************************/


#include "csf.h"
#include "csfimpl.h"

/* test if a value is missing value
 * returns 0 if not, nonzero if it is a missing value
 */
int  IsMV(
	const MAP *map, /* map handle */
	const void *cellValue) /* value to be tested */
{
	return(IsMVcellRepr(map->appCR, cellValue));
}

/* test if a value is missing value
 * returns 0 if not, nonzero if it is a missing value
 */
int  IsMVcellRepr(
	CSF_CR cellRepr,        /* cell representation of argument cellValue.
	                        * That is one of the constants prefixed by CR_.
	                        */
	const void *cellValue) /* value to be tested */
{

	if (IS_SIGNED(cellRepr))
	 	switch ( (cellRepr & CSF_SIZE_MV_MASK ) >> CSF_POS_SIZE_MV_MASK)
		{
		case 0:	return(*((const INT1 *)cellValue) == MV_INT1);
		case 1:	return(*((const INT2 *)cellValue) == MV_INT2);
		default:return(*((const INT4 *)cellValue) == MV_INT4);
		}
	else
		if (IS_REAL(cellRepr))
		{
			if (cellRepr == CR_REAL4)
				return(IS_MV_REAL4(cellValue));
			else
				return(IS_MV_REAL8(cellValue));
		}
		else
		{
			switch ( (cellRepr & CSF_SIZE_MV_MASK ) >> CSF_POS_SIZE_MV_MASK)
			{
			case 0:    return(*((const UINT1 *)cellValue) == MV_UINT1);
			case 1:	   return(*((const UINT2 *)cellValue) == MV_UINT2);
			default:   return(*((const UINT4 *)cellValue) == MV_UINT4);
			}
		}
}
