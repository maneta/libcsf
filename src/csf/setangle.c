/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * setangle.c 
   $Log: setangle.c,v $
   Revision 1.2  2000/02/05 21:25:48  cees
   added LOCATION_ATTRIBUTER struct

   Revision 1.1.1.1  2000/01/04 21:05:05  cees
   Initial import Cees

   Revision 2.0  1996/05/23 13:16:26  cees
   csf2clean

   Revision 1.1  1996/05/23 13:11:49  cees
   Initial revision

   Revision 1.2  1995/11/01 17:23:03  cees
   .

 * Revision 1.1  1994/09/07  13:23:08  cees
 * Initial revision
 *
 */
#ifndef lint  
static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/setangle.c,v 1.2 2000/02/05 21:25:48 cees Exp $";
#endif

/********/
/* USES */
/********/

/* libs ext. <>, our ""  */
#include <math.h>
#include "csf.h"
#include "csfimpl.h"

/* global header (opt.) and setangle's prototypes "" */


/* headers of this app. modules called */ 

/***************/
/* EXTERNALS   */
/***************/

/**********************/ 
/* LOCAL DECLARATIONS */
/**********************/ 

/*********************/ 
/* LOCAL DEFINITIONS */
/*********************/ 

/******************/
/* IMPLEMENTATION */
/******************/

/* Set the stuff in the header after header initialization (LIBRARY_INTERNAL)
 * Implements some common code for Mopen, Rcreate and family:
 *
 * set the map angle cosine and sin in header
 * these values are only used in the co-ordinate conversion
 * routines. And since they do a counter clockwise rotation we
 * take the sine and cosine of the negative angle.
 *
 * copy projection field into  raster, so raster can act as an 
 * indepent structure, for transformations
 */
void CsfFinishMapInit(
	MAP *m)		/* map handle */
{
	m->raster.angleCos   = cos(-(m->raster.angle));
	m->raster.angleSin   = sin(-(m->raster.angle));
	m->raster.projection = MgetProjection(m);
}
