/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * delattr.c
$Log: delattr.c,v $
Revision 1.1.1.1  2000/01/04 21:04:36  cees
Initial import Cees

Revision 2.1  1996/12/29 19:35:21  cees
src tree clean up

Revision 2.0  1996/05/23 13:16:26  cees
csf2clean

Revision 1.1  1996/05/23 13:11:49  cees
Initial revision

Revision 1.3  1995/11/01 17:23:03  cees
.

 * Revision 1.2  1994/09/08  17:16:23  cees
 * added c2man docs + small code changes
 *
 * Revision 1.1  1994/08/26  13:33:23  cees
 * Initial revision
 *
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/delattr.c,v 1.1.1.1 2000/01/04 21:04:36 cees Exp $";
#endif

#include "csf.h"
#include "csfimpl.h"

/* delete attribute from map
 * MdelAttribute deletes an attribute
 * from a map, if the attribute is available.
 * returns
 * the id argument if the attribute is succesfully deleted,
 * or 0 in case of error or if the attribute is not found.
 *
 * Merrno
 * NOACCESS
 * WRITE_ERROR
 */
CSF_ATTR_ID MdelAttribute(
	MAP *m,     /* map handle */
	CSF_ATTR_ID id)   /* identification of attribute */
{
	ATTR_CNTRL_BLOCK b;
	CSF_FADDR pos;

	if (! WRITE_ENABLE(m))
	{
		M_ERROR(NOACCESS);
		goto error;
	}

	pos = CsfGetAttrBlock(m, id, &b);  
	if (pos == 0)
		goto error;

	b.attrs[CsfGetAttrIndex(id, &b)].attrId = ATTR_NOT_USED;
	if (CsfWriteAttrBlock(m, pos, &b))
	{
		M_ERROR(WRITE_ERROR);
		goto error;
	}

	return id ;

error:	return 0 ;	/* not found or an error */
}
