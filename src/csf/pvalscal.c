/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * pvalscal.c
$Log: pvalscal.c,v $
Revision 1.1.1.1  2000/01/04 21:04:57  cees
Initial import Cees

Revision 2.1  1996/12/29 19:35:21  cees
src tree clean up

Revision 2.0  1996/05/23 13:16:26  cees
csf2clean

Revision 1.1  1996/05/23 13:11:49  cees
Initial revision

Revision 1.3  1995/11/01 17:23:03  cees
.

 * Revision 1.2  1994/09/08  17:16:23  cees
 * added c2man docs + small code changes
 *
 * Revision 1.1  1994/08/26  13:33:23  cees
 * Initial revision
 *
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/pvalscal.c,v 1.1.1.1 2000/01/04 21:04:57 cees Exp $";
#endif


#include "csf.h"
#include "csfimpl.h"

/* put new value scale
 * RputValueScale changes the value scale
 * of the map.
 *
 * returns
 *  new value scale or VS_UNDEFINED in case of an error.
 * 
 * NOTE
 * Note that there is no check if the cell representation
 * is complaint.
 *
 * Merrno
 * NOACCESS
 */
CSF_VS RputValueScale(
	MAP *map,         /* map handle */
	CSF_VS valueScale) /* new value scale */
{
	CHECKHANDLE_GOTO(map, error);
	if(! WRITE_ENABLE(map))
	{
		 M_ERROR(NOACCESS);
		 goto error;
	}
	map->raster.valueScale = valueScale;
	return(valueScale);
error:	return(VS_UNDEFINED);
}
