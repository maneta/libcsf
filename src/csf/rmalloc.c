/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * rmalloc.c 
 */

#ifndef lint  
static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/rmalloc.c,v 1.1.1.1 2000/01/04 21:04:59 cees Exp $";
#endif

#include "csf.h"
#include "csfimpl.h"

/* allocate dynamic memory large enough to hold in-file and app cells
 * Rmalloc allocates memory to hold  nrOfCells 
 * cells in both the in-file and app cell representation. Allocation
 * is done by malloc for other users. Our own (utrecht university) applications
 * calls ChkMalloc. Freeing memory allocated by Rmalloc is done by free (or Free).
 *
 * NOTE
 * Note that a possible RuseAs call must be done BEFORE Rmalloc.
 *
 * returns
 * a pointer the allocated memory or
 * NULL
 * if the request fails
 *
 * example
 * .so examples/_row.tr
 */
void *Rmalloc(
	const MAP *m,      /* map handle */
	size_t nrOfCells)   /* number of cells allocated memory must hold */
{
	CSF_CR inFileCR = RgetCellRepr(m);
	CSF_CR largestCellRepr = 
		LOG_CELLSIZE(m->appCR) > LOG_CELLSIZE(inFileCR) 
		 ?  m->appCR : inFileCR;

	return CSF_MALLOC((size_t)CSFSIZEOF(nrOfCells, largestCellRepr));
}
