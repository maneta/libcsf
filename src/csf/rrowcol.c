/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * rrowcol.c
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/rrowcol.c,v 1.3 2002/02/04 08:58:38 cees Exp $";
#endif

#include "csf.h"
#include "csfimpl.h"

#include <math.h> /* floor */

/* compute (fractional) row, column index from true world co-ordinate.
 * RasterCoords2RowCol computes row, column index from true world co-ordinate. 
 * The row and column co-ordinate are returned as fractions (See parameters
 * section).
 * The x,y co-ordinate
 * don't have to be on the map. They are just relative to upper left position.
 *
 * returns
 * 0 if the co-ordinate is outside the map,
 * 1 if inside,
 * -1 in case of an error
 *
 * Merrno
 * ILL_CELLSIZE
 */
void RasterCoords2RowCol(
 const CSF_RASTER_LOCATION_ATTRIBUTES *m,
 double x,      /* x of true co-ordinate */
 double y,      /* y of true co-ordinate */
 double *row,   /* write-only. Row index (y-pos). floor(row) is row number,
                 * if (row >= 0) then fmod(row, 1) is in-pixel displacement from pixel-top,
                 * if (row <0) then fmod(row, 1) is in-pixel displacement from pixel-bottom. 
                 */
 double *col)   /* write-only. Column index (x-pos). floor(col) is column number,
                 * if (col >= 0) then fmod(col, 1) is in-pixel displacement from pixel-left, 
                 * if (col <0) then fmod(col, 1) is in-pixel displacement from pixel-right.
                 */
{
	double cs = m->cellSize;
	double xCol = (x - m->xUL) / cs; 
	double yRow = (((m->projection == PT_YINCT2B) 
	                ? (y - m->yUL)
	                : (m->yUL - y)) / cs); 
	/* rotate clockwise: */
	double c = m->angleCos;    /* cos(t) == cos(-t) */
	double s = -(m->angleSin); /* -sin(t) == sin(-t) */
	*col  = xCol * c - yRow * s;
	*row  = xCol * s + yRow * c;
}

int RasterCoords2RowColChecked(
 const CSF_RASTER_LOCATION_ATTRIBUTES *m,
 double x,      /* x of true co-ordinate */
 double y,      /* y of true co-ordinate */
 double *row, 
 double *col)  
{
	double row_,col_; /* use copies, func( , , ,&dummy, &dummy) will fail */
	RasterCoords2RowCol(m,x,y,&row_,&col_);
  *row = row_;
  *col = col_;
	return( row_ >= 0 && col_ >= 0 && 
	        (m->nrRows > row_) && (m->nrCols > col_) );
}

/* compute (fractional) row, column index from true world co-ordinate.
 * Rcoord2RowCol computes row, column index from true world co-ordinate. 
 * The row and column co-ordinate are returned as fractions (See parameters
 * section).
 * The x,y co-ordinate
 * don't have to be on the map. They are just relative to upper left position.
 *
 * returns
 * 0 if the co-ordinate is outside the map,
 * 1 if inside,
 * -1 in case of an error
 *
 * Merrno
 * ILL_CELLSIZE
 */
int Rcoords2RowCol(
 const MAP *m,  /* map handle */
 double x,      /* x of true co-ordinate */
 double y,      /* y of true co-ordinate */
 double *row,   /* write-only. Row index (y-pos). floor(row) is row number,
                 * if (row >= 0) then fmod(row, 1) is in-pixel displacement from pixel-top,
                 * if (row <0) then fmod(row, 1) is in-pixel displacement from pixel-bottom. 
                 */
 double *col)   /* write-only. Column index (x-pos). floor(col) is column number,
                 * if (col >= 0) then fmod(col, 1) is in-pixel displacement from pixel-left, 
                 * if (col <0) then fmod(col, 1) is in-pixel displacement from pixel-right.
                 */
{
	double row_,col_; /* use copies, func( , , ,&dummy, &dummy) will fail
	                   * otherwise
	                   */
	if (m->raster.cellSize <= 0 
	    || (m->raster.cellSize != m->raster.cellSizeDupl ) )
	{ /* CW we should put this in Mopen */ 
		M_ERROR(ILL_CELLSIZE);
		goto error;
	}

	RasterCoords2RowCol(&(m->raster),x,y,&row_,&col_);
        *row = row_;
        *col = col_;
	return( row_ >= 0 && col_ >= 0 && 
	        (m->raster.nrRows > row_) && (m->raster.nrCols > col_) );
error:  return(-1);
}


/* compute row, column number of true world co-ordinate
 * RgetRowCol computes row, column number of true world co-ordinate.
 *
 * returns
 * 0  if the co-ordinate is outside the map,
 * 1 if inside,
 * -1 in case of an error
 *
 * Merrno
 * ILL_CELLSIZE
 */
int RgetRowCol(
	const MAP *m, /* map handle */
	double x,     /* x of true co-ordinate */
	double y,     /* y of true co-ordinate */
	size_t *row,   /* write-only. Row number (y-pos).
	               * Undefined if (x,y) is outside of map
	               */
	size_t *col)   /* write-only. Column number (x-pos).
	               * Undefined if (x,y) is outside of map
	               */
{
	double row_d,col_d;
	int    result;
	result = Rcoords2RowCol(m,x,y,&row_d,&col_d);
	if (result > 0)
	{
		*row = (size_t)floor(row_d);
		*col = (size_t)floor(col_d);
	}
	return(result);
}
