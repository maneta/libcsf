/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * mperror.c
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/mperror.c,v 1.1.1.1 2000/01/04 21:04:52 cees Exp $";
#endif

#include "csf.h"
#include "csfimpl.h"


static const char *errolist[ERRORNO]={
"No error",
"File could not be opened or does not exist",
"File is not a PCRaster file",
"Wrong C.S.F.-version",
"Wrong byte order",
"Not enough memory",
"Illegal cell representation constant",
"Acces denied",
"Row number to big",
"Collumn number to big",
"Map is not a raster file",
"Illegal conversion",
"No space on device to write",
"A write error occurred",
"Illegal handle",
"A read error occurred",
"Illegal access mode constant",
"Attribute not found",
"Attribute already in file",
"Cell size <= 0",
"Conflict between cell representation and value scale",
"Illegal value scale",
"XXXXXXXXXXXXXXXXXXXX",
"Angle < -0.5 pi or > 0.5 pi",
"Can't read as a boolean map",
"Can't write as a boolean map",
"Can't write as a ldd map",
"Can't use as a ldd map",
"Can't write to version 1 cell representation",
"Usetype is not version 2 cell representation, VS_LDD or VS_BOOLEAN"
};

/* write error message to stderr
 * Mperror writes the error message belonging to the current Merrno
 * value to stderr, prefixed by a userString, separated by a semicolon.
 *
 * example
 * .so examples/csfdump1.tr
 */
void Mperror(
	const char *userString) /* prefix string */
{
	(void)fprintf(stderr,"%s : %s\n", userString, errolist[Merrno]);
}

/* write error message to stderr and exits
 * Mperror first writes the error message belonging to the current Merrno
 * value to stderr, prefixed by userString, separated by a semicolon.
 * Then Mperror exits by calling exit() with the given exit code.
 *
 * returns
 * NEVER RETURNS!
 *
 * example
 * .so examples/csfdump2.tr
 */
void MperrorExit(
	const char *userString, /* prefix string */
	int exitCode) /* exit code */
{
	Mperror(userString);
	exit(exitCode);
}

/* error message 
 * MstrError returns the error message belonging to the current Merrno
 * value.
 * returns the error message belonging to the current Merrno
 *
 * example
 * .so examples/testcsf.tr
 */
const char *MstrError(void)
{	
	return(errolist[Merrno]);
}
