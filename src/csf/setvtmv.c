/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * setvtmv.c
$Log: setvtmv.c,v $
Revision 1.1.1.1  2000/01/04 21:05:06  cees
Initial import Cees

Revision 2.0  1996/05/23 13:16:26  cees
csf2clean

Revision 1.1  1996/05/23 13:11:49  cees
Initial revision

Revision 1.2  1995/11/01 17:23:03  cees
.

 * Revision 1.1  1994/08/26  13:33:23  cees
 * Initial revision
 *
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/setvtmv.c,v 1.1.1.1 2000/01/04 21:05:06 cees Exp $";
#endif


#include "csf.h"
#include "csfimpl.h"

/* (LIBRARY_INTERNAL)
 */
void CsfSetVarTypeMV( CSF_VAR_TYPE *var, CSF_CR cellRepr)
{
/* assuming unions are left-alligned */
	if(IS_SIGNED(cellRepr))
		switch(LOG_CELLSIZE(cellRepr))
		{
		 case 2	:	*(INT4 *)var = MV_INT4;
		 		break;
		 case 1	:	*(INT2 *)var = MV_INT2;
		 		break;
		 default:	POSTCOND(LOG_CELLSIZE(cellRepr) == 0);
		 		*(INT1 *)var = MV_INT1;
		}
	else
	{
		((UINT4 *)var)[0] = MV_UINT4;
		((UINT4 *)var)[1] = MV_UINT4;
	}
}
