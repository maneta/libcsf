/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/putallmv.c,v 1.1.1.1 2000/01/04 21:04:53 cees Exp $";
#endif

#include <stdlib.h>

#include "csf.h"
#include "csfimpl.h"

/* make all cells missing value in map
 * RputAllMV writes a missing values to all the cells in a
 * map. For this is allocates a buffer to hold one row at a
 * time.
 * returns 1 if succesfull, 0 in case of an error
 */
int RputAllMV(
	MAP *m)
{
	size_t i,nc,nr;
	void *buffer;
	CSF_CR cr;

	CHECKHANDLE_GOTO(m, error);
	if(! WRITE_ENABLE(m))
	{
		M_ERROR(NOACCESS);
		goto error;
	}

	cr = RgetCellRepr(m);
	nc = RgetNrCols(m);

	buffer = Rmalloc(m,nc);
	if(buffer == NULL)
	{
		M_ERROR(NOCORE);
		goto error;
	}

	/*  Fill buffer with determined Missingvalue*/
	SetMemMV(buffer, nc, cr);

	nr = RgetNrRows(m);
	for(i = 0 ; i < nr; i++)
	 if (RputRow(m, i, buffer) != nc)
	 {
	    M_ERROR(WRITE_ERROR);
	    goto error_f;
	 }
	CSF_FREE(buffer);

	CsfSetVarTypeMV( &(m->raster.minVal), cr);
	CsfSetVarTypeMV( &(m->raster.maxVal), cr);

	return(1);
error_f:  
	CSF_FREE(buffer);
error:
	return(0);
}
