/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * gdattype.c
$Log: gdattype.c,v $
Revision 1.1.1.1  2000/01/04 21:04:37  cees
Initial import Cees

Revision 2.1  1996/12/29 19:35:21  cees
src tree clean up

Revision 2.0  1996/05/23 13:16:26  cees
csf2clean

Revision 1.1  1996/05/23 13:11:49  cees
Initial revision

Revision 1.3  1995/11/01 17:23:03  cees
.

 * Revision 1.2  1994/09/02  16:22:20  cees
 * added c2man doc
 * const'ified map handle
 *
 * Revision 1.1  1994/08/26  13:33:23  cees
 * Initial revision
 *
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/gdattype.c,v 1.1.1.1 2000/01/04 21:04:37 cees Exp $";
#endif


#include "csf.h"
#include "csfimpl.h"

/* data type of the map
 * MgetDataType returns the type of the map, which is always
 * a raster until now.
 * returns
 * T_RASTER
 */
UINT4 MgetMapDataType(
	const MAP *map) /* map handle */
{
	return (UINT4)(map->main.mapType);
}
