/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * _getrow.c
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/_getrow.c,v 1.1.1.1 2000/01/04 21:04:09 cees Exp $";
#endif

#include "csf.h"
#include "csfimpl.h"

/* read one row from a CSF raster file
 * RgetRow reads one row of cells from a
 * file. 
 * returns
 * Number of cells successfully read
 *
 * example
 * .so examples/_row.tr
 */
size_t RgetRow(
	MAP *map,        /* map handle */
	size_t rowNr,     /* row number to be read */
	void *buf)       /* write-only. buffer large enough to hold
	                  * cell values of one row in both the file
	                  * and in-app cell representation
	                  */
{
	return RgetSomeCells(map,
				map->raster.nrCols*rowNr,
				map->raster.nrCols, 
				buf) ;
}
