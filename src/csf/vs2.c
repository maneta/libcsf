/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef lint  
static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/vs2.c,v 1.1.1.1 2000/01/04 21:05:11 cees Exp $";
#endif

/********/
/* USES */
/********/

/* libs ext. <>, our ""  */

/* global header (opt.) and vsis's prototypes "" */
#include "csf.h" 
#include "csfimpl.h" 


/* headers of this app. modules called */ 

/***************/
/* EXTERNALS   */
/***************/

/**********************/ 
/* LOCAL DECLARATIONS */
/**********************/ 

/*********************/ 
/* LOCAL DEFINITIONS */
/*********************/ 

/******************/
/* IMPLEMENTATION */
/******************/

/* test if valuescale/datatype is a CSF version 2 value scale
 * RvalueScale2 tests if the map's value scale is a version
 * 2 valuescale/datatype.
 * returns
 * 0 if the value is not in the above list, 1 if it does.
 *
 */
int RvalueScale2(
	CSF_VS    vs) /* value scale. ALL OF BELOW are accepted */
{
	switch(vs) {
	  case VS_LDD: 
	 case VS_BOOLEAN:
	 case VS_NOMINAL:
	 case VS_ORDINAL: 
	 case VS_SCALAR: 
	 case VS_DIRECTION: return 1;
	 default :       return 0;
      }
}
