/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * rdup2.c 
 */
#ifndef lint  
static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/rdup2.c,v 1.2 2000/02/05 21:25:48 cees Exp $";
#endif

#include "csf.h"
#include "csfimpl.h"


/* create a new map by cloning another one
 * Rdup creates a new empty map from the specifications of another map.
 * No cell values are copied. It uses a call to Rcreate to create the
 * map. See Rcreate for legal values of the args cellRepr and valueScale.
 * returns the map handle of the newly created map or NULL in case of an
 * error
 *
 * Merrno
 * NOT_RASTER plus the Merrno codes of Rcreate
 *
 * EXAMPLE
 * .so examples/dupbool.tr
 */
MAP  *Rdup(
	const char *toFile, /* file name of map to be created */
	const MAP *from,    /* map to clone from */
	CSF_CR cellRepr,     /* cell representation of new map  */
	CSF_VS dataType)   /* datatype/valuescale of new map  */
{
	MAP *newMap = NULL; /* set NULL for goto error */

	CHECKHANDLE_GOTO(from, error);

	/* check if mapType is T_RASTER */
	if (from->main.mapType != T_RASTER)
	{
		M_ERROR(NOT_RASTER);
		goto error;
	}

	newMap = Rcreate(toFile,
	                 from->raster.nrRows,
			 from->raster.nrCols,
        		 cellRepr, 
        		 dataType, 
        		 from->main.projection, 
        		 from->raster.xUL,
        		 from->raster.yUL,
        		 from->raster.angle,
        		 from->raster.cellSize);

error:
	return newMap ;
}
