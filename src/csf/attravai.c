/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * attravai.c
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/attravai.c,v 1.1.1.1 2000/01/04 21:04:11 cees Exp $";
#endif

#include "csf.h"
#include "csfimpl.h"

/* check if an attribute is available
 * MattributeAvail search for the given id in the map.
 *
 * returns
 *  0 if the attribute is not available,
 *  nonzero if the attribute is available
 *
 * Merrno
 * ILLHANDLE
 */
int MattributeAvail(
	MAP *m,    /* map handle */
	CSF_ATTR_ID id)  /* identification of attribute */
{
	ATTR_CNTRL_BLOCK b;

     	if (! CsfIsValidMap(m))
	 	return 0;
	return(CsfGetAttrBlock(m, id, &b) != 0);
}
