/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * _gsomece.c
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/_gsomece.c,v 1.1.1.1 2000/01/04 21:04:10 cees Exp $";
#endif


#include "csf.h"
#include "csfimpl.h"

/* read a stream of cells
 * RgetSomeCells views a raster as one linear stream of
 * cells, with row i+1 placed after row i. 
 * In this stream any sequence can be read by specifying an
 * offset and the number of cells to be read
 * returns the number of cells read, just as fread
 *
 * example
 * .so examples/somecell.tr
 */
size_t RgetSomeCells(
	MAP *map,	/* map handle */
	size_t offset,   /* offset from pixel (row,col) = (0,0) */
	size_t nrCells,  /* number of cells to be read */
	void *buf)/* write-only. Buffer large enough to
                   * hold nrCells cells in the in-file cell representation
                   * or the in-app cell representation.
                   */
{

	CSF_FADDR  readAt;
	size_t cellsRead;
	UINT2  inFileCR = RgetCellRepr(map);

	offset <<= LOG_CELLSIZE(inFileCR);
	readAt = ADDR_DATA + (CSF_FADDR)offset;
	fseek(map->fp, (long)readAt, SEEK_SET);
	cellsRead = map->read(buf, (size_t)CELLSIZE(inFileCR),
	(size_t)nrCells, map->fp);

	PRECOND(map->file2app != NULL);
	map->file2app(nrCells, buf);

	return(cellsRead);
}
