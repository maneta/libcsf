/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * cellsize.c 
   $Log: cellsize.c,v $
   Revision 1.2  2000/02/05 21:25:48  cees
   added LOCATION_ATTRIBUTER struct

   Revision 1.1.1.1  2000/01/04 21:04:12  cees
   Initial import Cees

   Revision 2.0  1996/05/23 13:16:26  cees
   csf2clean

   Revision 1.1  1996/05/23 13:11:49  cees
   Initial revision

   Revision 1.2  1995/11/01 17:23:03  cees
   .

 * Revision 1.1  1994/09/08  17:16:23  cees
 * Initial revision
 *
 */
#ifndef lint  
static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/cellsize.c,v 1.2 2000/02/05 21:25:48 cees Exp $";
#endif

/********/
/* USES */
/********/

/* libs ext. <>, our ""  */
#include "csf.h"
#include "csfimpl.h"

/* global header (opt.) and cellsize's prototypes "" */


/* headers of this app. modules called */ 

/***************/
/* EXTERNALS   */
/***************/

/**********************/ 
/* LOCAL DECLARATIONS */
/**********************/ 

/*********************/ 
/* LOCAL DEFINITIONS */
/*********************/ 

/******************/
/* IMPLEMENTATION */
/******************/


/* get cell size 
 * returns the cell size or -1 in case of an error
 *
 * Merrno
 *  ILL_CELLSIZE
 *  ILLHANDLE
 */
REAL8 RgetCellSize(
	const MAP *map) /* map handle */
{
	CHECKHANDLE(map);
	if ( map->raster.cellSize != map->raster.cellSizeDupl)
	{
		M_ERROR(ILL_CELLSIZE);
		return -1;
	}

	return(map->raster.cellSize);
}

/* put cell size
 * returns the new cell size or -1
 * in case of an error.
 *
 * Merrno
 * ILLHANDLE
 * NOACCESS
 * ILL_CELLSIZE
 */
REAL8 RputCellSize(
	MAP *map, /* map handle */
	REAL8 cellSize) /* new cell size */
{
	CHECKHANDLE_GOTO(map, error);
	if(! WRITE_ENABLE(map))
	{
		M_ERROR(NOACCESS);
		goto error;
	}
	if (cellSize <= 0.0)
	{
		M_ERROR(ILL_CELLSIZE);
		goto error;
	}
	map->raster.cellSize     = cellSize;
	map->raster.cellSizeDupl = cellSize;
	return(cellSize);
error:  return(-1.0);
}
