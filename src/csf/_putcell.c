/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * _putcell.c
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/_putcell.c,v 1.1.1.1 2000/01/04 21:04:10 cees Exp $";
#endif

#include "csf.h"
#include "csfimpl.h"

/* write one cell to a CSF raster file
 * RputCell writes one cell value to a
 * file. 
 * returns
 * 1 if cell is successfully written, not 1 if not.
 *
 * example
 * .so examples/rawbin.tr
 */
size_t RputCell(
MAP *map,         /* map handle */
size_t rowNr,      /* Row number of cell */
size_t colNr,      /* Column number of cell */
void *cellValue)  /* read-write. Buffer large enough to
                   * hold one cell in the in-file cell representation
                   * or the in-app cell representation.
                   * If these types are not equal then the buffer is
                   * converted from the in-app to the in-file 
                   * cell representation. 
                   */
{
	return RputSomeCells(map, 
	                      (map->raster.nrCols * rowNr) + colNr,
	                      1, cellValue) ;
}
