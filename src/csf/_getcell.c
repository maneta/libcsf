/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * _getcell.c
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/_getcell.c,v 1.1.1.1 2000/01/04 21:04:08 cees Exp $";
#endif

#include "csf.h"
#include "csfimpl.h"

/* read one cell from a CSF raster file
 * RgetCell reads one cell value from a
 * file. 
 * returns
 * 1 if cell is successfully read,
 * 0 if not
 *
 * example
 * .so examples/csfdump1.tr
 */
size_t RgetCell(
	MAP *map,        /* map handle */
	size_t rowNr,     /* row number of cell */
	size_t colNr,     /* column number of cell */
	void *cellValue) /* write-only. buffer, large enough to hold
	                  * the value of the cell in the file and app
	                  * cell representation
	                  */
{
	return RgetSomeCells(map, 
				( (map->raster.nrCols) * rowNr) + colNr,
	             		1, cellValue);
}
