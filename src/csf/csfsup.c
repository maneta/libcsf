/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * csfsup.c
$Log: csfsup.c,v $
Revision 1.1.1.1  2000/01/04 21:04:34  cees
Initial import Cees

Revision 2.0  1996/05/23 13:16:26  cees
csf2clean

Revision 1.1  1996/05/23 13:11:49  cees
Initial revision

Revision 1.3  1995/11/01 17:23:03  cees
.

 * Revision 1.2  1994/09/06  13:21:04  cees
 * added c2man docs
 *
 * Revision 1.1  1994/08/26  13:33:23  cees
 * Initial revision
 *
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/csfsup.c,v 1.1.1.1 2000/01/04 21:04:34 cees Exp $";
#endif

#include "csf.h"
#include "csftypes.h"
#include <string.h>    /* memset */

/* set an array of cells to missing value
 * SetMemMV sets an array of cells to missing value
 */
void SetMemMV(
	void *buf,		/* write-only buffer with cells */
	size_t nrElements,      /* number of cells */
	CSF_CR cellRepr)         /* cell representation */
{
size_t index;

	switch (cellRepr) {
	  case CR_INT1: (void)memset(buf,MV_INT1,nrElements);break;
	  case CR_INT2: for (index=0;index<nrElements;index++)
					 ((INT2 *) buf)[index]=MV_INT2;
			break;
	  case CR_INT4: for (index=0;index<nrElements;index++)
					 ((INT4 *) buf)[index]=MV_INT4;
			break;
	  default: (void)memset(buf,MV_UINT1,CSFSIZEOF(nrElements,cellRepr));
	}
}
