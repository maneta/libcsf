/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/gcellrep.c,v 1.2 2000/02/20 15:23:06 cees Exp $";
#endif


#include "csf.h"
#include "csfimpl.h"

/* get cell representation
 * RgetCellRepr returns the in-file cell representation.
 * returns the cell representation 
 */
CSF_CR RgetCellRepr(
	const MAP *map) /* map handle */
{
	return(map->raster.cellRepr);
}

/* get cell representation as set by RuseAs
 * RgetUseCellRepr returns the cell representation as set by RuseAs
 * returns the cell representation as set by RuseAs
 */

CSF_CR RgetUseCellRepr(
	const MAP *map) /* map handle */
{
   	return(map->appCR);
}
