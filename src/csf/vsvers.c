/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * vsvers.c 
 */
#ifndef lint  
static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/vsvers.c,v 1.1.1.1 2000/01/04 21:05:14 cees Exp $";
#endif

/********/
/* USES */
/********/

/* libs ext. <>, our ""  */
#include "csf.h" 

/* global header (opt.) and vsvers's prototypes "" */


/* headers of this app. modules called */ 

/***************/
/* EXTERNALS   */
/***************/

/**********************/ 
/* LOCAL DECLARATIONS */
/**********************/ 

/*********************/ 
/* LOCAL DEFINITIONS */
/*********************/ 

/******************/
/* IMPLEMENTATION */
/******************/

/* get version nr of value scale
 * returns
 * 0 for illegal value scale,
 * 1 version 1 value scale,
 * 2 version 2 value scale
 */
int RgetValueScaleVersion(
	const MAP *m) /* map handle */
{
	UINT2 vs = RgetValueScale(m);
	
	switch(vs) {
	  case VS_CLASSIFIED   : 
	  case VS_CONTINUOUS   : 
	  case VS_NOTDETERMINED: return 1;
	  case VS_LDD      : 
	  case VS_BOOLEAN  :
	  case VS_NOMINAL  :
	  case VS_ORDINAL  : 
	  case VS_SCALAR   :
	  case VS_DIRECTION: return 2;
	  default          : return 0;
      }
}
