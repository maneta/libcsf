/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * attrsize.c
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/attrsize.c,v 1.1.1.1 2000/01/04 21:04:11 cees Exp $";
#endif

#include "csf.h"
#include "csfimpl.h"

/* get the size of an attribute (LIBRARY_INTERNAL)
 * returns
 * 0 if the attribute is not available,
 * or the nonzero size if the attribute is available.
 */
size_t CsfAttributeSize(
	 MAP   *m,    /* map handle */
	 CSF_ATTR_ID id)    /* identification of attribute */
{
	ATTR_CNTRL_BLOCK b;

	if (CsfGetAttrBlock(m, id, &b) != 0)
		return b.attrs[CsfGetAttrIndex(id, &b)].attrSize;
        return 0;
}
