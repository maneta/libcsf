/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * gety0.c
$Log: gety0.c,v $
Revision 1.1.1.1  2000/01/04 21:04:44  cees
Initial import Cees

Revision 2.0  1996/05/23 13:16:26  cees
csf2clean

Revision 1.1  1996/05/23 13:11:49  cees
Initial revision

Revision 1.3  1995/11/01 17:23:03  cees
.

 * Revision 1.2  1994/09/08  17:16:23  cees
 * added c2man docs + small code changes
 *
 * Revision 1.1  1994/08/26  13:33:23  cees
 * Initial revision
 *
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/gety0.c,v 1.1.1.1 2000/01/04 21:04:44 cees Exp $";
#endif


#include "csf.h"
#include "csfimpl.h"

/* y value, upper left co-ordinate of map
 * RgetYUL returns the y value of the upper left co-ordinate of map.
 * Whether this is the largest or smallest y value depends on the
 * projection (See MgetProjection()).
 * returns the y value, upper left co-ordinate of map
 */
REAL8 RgetYUL(
	const MAP *map) /* map handle */
{
	CHECKHANDLE(map);
	return(map->raster.yUL);
}
