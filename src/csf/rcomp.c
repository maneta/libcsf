/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * rcomp.c
 */
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/rcomp.c,v 1.2 2000/02/05 21:25:48 cees Exp $";
#endif


/*****************************************************************/
/*    FUNCTION :  RCOMPARE                                       */
/*****************************************************************/
/*             				                         */
/*****************************************************************/


#include "csf.h"
#include "csfimpl.h"

/* compare 2 maps for their location attributes
 * Rcompare compares 2 maps for all location attributes:
 *
 * projection,
 *
 * xUL, yUL, angle,
 *
 * cell size and
 *
 * number of rows and columns
 *
 * returns 0 if one of these attributes differ or in case of an error, 1 
 * if they are all equal.
 *
 * Merrno
 * NOT_RASTER
 */
int Rcompare(
	const MAP *m1, /* map handle 1 */
	const MAP *m2) /* map handle 2 */
{
	CHECKHANDLE_GOTO(m1, error);

	/* check if mapType is T_RASTER */
	if ((m1->main.mapType != T_RASTER)
	|| (m2->main.mapType != T_RASTER))
	{
		M_ERROR(NOT_RASTER);
		goto error;
	}

	if (
	    MgetProjection(m1) == MgetProjection(m2) && 
	    m1->raster.xUL == m2->raster.xUL &&
    	    m1->raster.yUL == m2->raster.yUL &&
	    m1->raster.cellSize == m2->raster.cellSize &&
	    m1->raster.cellSizeDupl == m2->raster.cellSizeDupl &&
	    m1->raster.angle == m2->raster.angle &&
	    m1->raster.nrRows == m2->raster.nrRows &&
	    m1->raster.nrCols == m2->raster.nrCols 
	)	return(1);
error: 
		return(0);
}

int RgetLocationAttributes(
	CSF_RASTER_LOCATION_ATTRIBUTES *l, /* fill in this struct */
	const MAP *m) /* map handle to copy from */
{
	CHECKHANDLE_GOTO(m, error);
	*l = m->raster;
	return 1;
error:
	return 0;
}

int RcompareLocationAttributes(
	const CSF_RASTER_LOCATION_ATTRIBUTES *m1, /* */
	const CSF_RASTER_LOCATION_ATTRIBUTES *m2) /* */
{
	return  (
	    m1->projection == m2->projection && 
	    m1->xUL == m2->xUL && m1->yUL == m2->yUL &&
	    m1->cellSize == m2->cellSize &&
	    m1->angle == m2->angle &&
	    m1->nrRows == m2->nrRows && m1->nrCols == m2->nrCols );
}
