/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/gmaxval.c,v 1.1.1.1 2000/01/04 21:04:44 cees Exp $";
#endif

#include "csf.h"
#include "csfimpl.h"

/* get maximum cell value
 * RgetMaxVal returns the value stored in
 * the header as the maximum value. 
 * If the minMaxStatus is MM_WRONGVALUE
 * then a missing value is returned.
 * returns 0 if argument maxVal is returned with a missing
 * value, nonzero if not.
 *
 * example
 * .so examples/csfstat.tr
 */
int RgetMaxVal(
	const MAP *map, /* map handle */
	void *maxVal)   /* write-only. Maximum value or missing value */
{
	/* use buffer that can hold largest 
	 * cell representation
	 */
	CSF_VAR_TYPE buf_1;
	void *buf = (void *)(&buf_1);

	CHECKHANDLE(map);
	CsfGetVarType(buf, &(map->raster.maxVal), RgetCellRepr(map));

	map->file2app(1, buf);

	if (map->minMaxStatus == MM_WRONGVALUE)
		SetMV(map, buf);

	CsfGetVarType(maxVal, buf, map->appCR);

	return((!IsMV(map,maxVal)) &&
 		map->minMaxStatus!=MM_WRONGVALUE);
}
