/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef lint
 static const char *rcs_id = 
 "$Header: /home/cvs/pcrteam/pcrtree/libs/csf/endian.c,v 1.1.1.1 2000/01/04 21:04:37 cees Exp $";
#endif

#include "csf.h"
#include "csfimpl.h"

/* test if map is in native endian mode
 * test if map is in native endian mode
 * returns nonzero if native, 0 if not native
 */
int    MnativeEndian(const MAP *map)
{
	return map->main.byteOrder == ORD_OK;
}

/* test if the Rput functions alter their cell buffers
 * test if the Rput functions alter their cell buffers
 * returns nonzero if they do not alter, 0 if they alter their buffers
 */
int    RputDoNotChangeValues(const MAP *map)
{
	return MnativeEndian(map) && map->appCR == map->raster.cellRepr;
}
